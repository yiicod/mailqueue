<?php

namespace yiicod\mailqueue\components;

use CApplicationComponent;
use CConsoleApplication;
use CDbCriteria;
use CLogger;
use Yii;

class MailQueue extends CApplicationComponent
{
    /**
     * @var type
     */
    public $afterSendDelete = false;

    /**
     * @var type
     */
    public $partSize = 50;

    /**
     * Push mass
     * array(
     *    array(
     *      'field name to' => '',
     *      'field name subject' => '',
     *      'field name body' => '',
     *      'field name priority' => '',
     *      'field name from' => '',
     *      'field name attachs' => '',
     *    )
     * ).
     *
     * @param array $data
     *
     * @return int Return int
     */
    public function pushMass($data)
    {
        $table = Yii::app()->getComponent('mailqueue')->modelMap['MailQueue']['class'];
        $model = new $table();

        $prepareData = [];
        $prepareMessages = [];
        $index = 1;
        foreach ($data as $item) {
            if (is_array($item)) {
                $prepareData = \CMap::mergeArray([
                        $model->fieldFrom => '',
                        $model->fieldTo => '',
                        $model->fieldSubject => '',
                        $model->fieldBody => '',
                        $model->fieldAttachs => [],
                        $model->fieldStatus => Yii::app()->getComponent('mailqueue')->modelMap['MailQueue']['status']['unsend'],
                        ], $item);
                $prepareData[$model->fieldAttachs] = \CJSON::encode($prepareData[$model->fieldAttachs]);
                if (in_array($model->fieldCreateDate, $model->attributeNames())) {
                    $prepareData[$model->fieldCreateDate] = date('Y-m-d H:i:s');
                }
                $prepareMessages[] = $prepareData;
            }
            if (($index % $this->partSize === 0 || $index >= count($data)) && false === empty($prepareMessages)) {
                //Reconnect for big duration
                Yii::app()->db->setActive(false);
                Yii::app()->db->setActive(true);
                Yii::app()->db->commandBuilder->createMultipleInsertCommand($model->tableName(), $prepareMessages)->execute();
                $prepareMessages = [];
            }
            ++$index;
        }
        //Reconnect for db stable works 
        Yii::app()->db->setActive(false);
        Yii::app()->db->setActive(true);
    }

    /**
     * Add mail from queue.
     *
     * @param string $to      Email to
     * @param string $subject Email subject
     * @param string Body email, html
     * @param string|array From email
     * @param string Attach for email array('path' => 'file path', 'name' => 'file bname')
     * 
     * @return bool Save or not
     */
    public function push($to, $subject, $body, $priority = 0, $from = '', array $attachs = [], $additionalFields = [])
    {
        $table = Yii::app()->getComponent('mailqueue')->modelMap['MailQueue']['class'];
        $model = new $table();

        $model->from = $from;
        $model->to = $to;
        $model->subject = $subject;
        $model->body = $body;
        $model->setAttachs($attachs);
        $model->status = Yii::app()->getComponent('mailqueue')->modelMap['MailQueue']['status']['unsend'];

        if (in_array($model->fieldPriority, $model->attributeNames())) {
            $model->priority = $priority;
        }

        foreach ($additionalFields as $field => $value) {
            $model->{$field} = $value;
        }

        return $model->save();
    }

    /**
     * Send mail from queue.
     *
     * @param CDbCriteria
     */
    public function delivery($criteria)
    {
        $table = Yii::app()->getComponent('mailqueue')->modelMap['MailQueue']['class'];
        $item = null;
        $ids = [];
        $failedIds = [];
        $deliveringCount = $criteria->limit;
        $statusSended = isset(Yii::app()->getComponent('mailqueue')->modelMap['MailQueue']['status']['send']) ?
            Yii::app()->getComponent('mailqueue')->modelMap['MailQueue']['status']['send'] :
            Yii::app()->getComponent('mailqueue')->modelMap['MailQueue']['status']['sended'];
        $statusUnsended = isset(Yii::app()->getComponent('mailqueue')->modelMap['MailQueue']['status']['unsend']) ?
            Yii::app()->getComponent('mailqueue')->modelMap['MailQueue']['status']['unsend'] :
            Yii::app()->getComponent('mailqueue')->modelMap['MailQueue']['status']['unsended'];
        $statusFailed = Yii::app()->getComponent('mailqueue')->modelMap['MailQueue']['status']['failed'];
        $fieldStatus = $table::model()->fieldStatus;

        while ($deliveringCount > 0) {
            $criteria->limit = min($this->partSize, $deliveringCount);
            $models = $table::model()->findAll($criteria);

            if (method_exists(Yii::app()->{Yii::app()->getComponent('mailqueue')->mailer}, 'deliveryBegin')) {
                Yii::app()->{Yii::app()->getComponent('mailqueue')->mailer}->deliveryBegin($models);
            }

            foreach ($models as $item) {
                $to = $item->to;
                $subject = $item->subject;
                $body = $item->body;
                $attachs = $item->getAttachs();
                $from = $item->from;
                if (method_exists(Yii::app()->{Yii::app()->getComponent('mailqueue')->mailer}, 'deliveryItemBegin')) {
                    Yii::app()->{Yii::app()->getComponent('mailqueue')->mailer}->deliveryItemBegin($item);
                }
                if ($isSuccess = Yii::app()->{Yii::app()->getComponent('mailqueue')->mailer}->send($to, $subject, $body, $from, $attachs)) {
                    $ids[] = $item->id;
                } else {
                    if (YII_DEBUG && Yii::app() instanceof CConsoleApplication) {
                        echo "MailQueue send false to - $to, subject - $subject \n";
                    }
                    Yii::log("MailQueue send false to - $to, subject - $subject \n", CLogger::LEVEL_ERROR, 'system.mailqueue');
                    $failedIds[] = $item->id;
                }
                if (method_exists(Yii::app()->{Yii::app()->getComponent('mailqueue')->mailer}, 'deliveryItemEnd')) {
                    Yii::app()->{Yii::app()->getComponent('mailqueue')->mailer}->deliveryItemEnd($isSuccess, $item);
                }
            }

            if (method_exists(Yii::app()->{Yii::app()->getComponent('mailqueue')->mailer}, 'deliveryEnd')) {
                Yii::app()->{Yii::app()->getComponent('mailqueue')->mailer}->deliveryEnd($ids, $failedIds);
            }

            if (count($ids)) {
                if ($this->afterSendDelete) {
                    $table::model()->deleteAll($criteria);
                } elseif (in_array($fieldStatus, $item->attributeNames())) {
                    $status = $statusSended;
                    $this->updateMailQueue($ids, $status);
                }
            }
            if (count($failedIds) && in_array($fieldStatus, $item->attributeNames())) {
                $status = $statusUnsended;
                if ($statusFailed != $statusUnsended) {
                    $status = $statusFailed;
                }
                $this->updateMailQueue($failedIds, $status);
            }

            $deliveringCount = $deliveringCount - $this->partSize;
        }
    }

    protected function updateMailQueue($ids, $status)
    {
        $table = Yii::app()->getComponent('mailqueue')->modelMap['MailQueue']['class'];
        $fieldStatus = $table::model()->fieldStatus;
        $fieldUpdate = $table::model()->fieldUpdateDate;

        $criteria = new CDbCriteria();
        $criteria->addInCondition('id', $ids);
        if (in_array($fieldUpdate, $table::model()->attributeNames())) {
            $table::model()->updateAll([
                $fieldStatus => $status,
                $fieldUpdate => date('Y-m-d H:i:s'),
                ], $criteria);
        } else {
            $table::model()->updateAll([
                $fieldStatus => $status,
                ], $criteria);
        }
    }
}
